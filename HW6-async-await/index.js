// Завдання
// Написати програму "Я тебе знайду по IP"

// Технічні вимоги:
// Створити просту HTML-сторінку з кнопкою Знайти по IP.
// Натиснувши кнопку - надіслати AJAX запит за адресою https://api.ipify.org/?format=json, отримати звідти IP адресу клієнта.
// Дізнавшись IP адресу, надіслати запит на сервіс https://ip-api.com/ та отримати інформацію про фізичну адресу.
// під кнопкою вивести на сторінку інформацію, отриману з останнього запиту – континент, країна, регіон, місто, район.
// Усі запити на сервер необхідно виконати за допомогою async await.

function findByIp() {
  const btn = document.querySelector('.button');

  btn.addEventListener('click', async () => {
    document.querySelector('.wrapper').innerHTML = '';
    const res = await fetch('https://api.ipify.org/?format=json');
    const data = await res.json();

    const getRes = await fetch(
      'http://ip-api.com/json/' + data.ip + '?fields=1581077'
    );
    const getData = await getRes.json();

    renderHtml(getData);
  });
}

findByIp();

function renderHtml(objForRender) {
  document.querySelector('.wrapper').insertAdjacentHTML(
    'afterbegin',
    `
  <ul class="ul">
  <li>Continent: ${objForRender.continent}</li>
  <li>Country: ${objForRender.country}</li>
  <li>City: ${objForRender.city}</li>
  <li>Region: ${objForRender.region}</li>
  <li>District: ${objForRender.district}</li>
  <li>IP: ${objForRender.query}</li>
  </ul>
  `
  );
}
