class Card {
  constructor() {
    this.root = document.getElementById('root');
  }

  async getUsersPosts() {
    const resposnUsers = await fetch(
      'https://ajax.test-danit.com/api/json/users'
    );
    const resUsers = resposnUsers.json();
    const users = await resUsers;
    console.log(users);
    const resposnPosts = await fetch(
      'https://ajax.test-danit.com/api/json/posts'
    );
    const resPosts = resposnPosts.json();
    const posts = await resPosts;
    const results = await Promise.all([users, posts]);
    console.log(results);
    this.createPosts(results);
  }

  createPosts([users, posts]) {
    posts.map((post) => {
      const user = users.find((el) => {
        return post.userId === el.id;
      });
      this.root.insertAdjacentHTML(
        'afterbegin',
        `
    <div class="post">
      <div class="img"><img src="./images/twitter.png" alt="twitter" /></div>
      <ul>
        <li class="h2">${post.title}</li>
        <li class="user">${user.name} </li>
        <li>${user.email}</li>
        <li>${post.body}</li>
      </ul>
      <button data-id="${post.id}" class="btn">DELETE</button>
    </div>
        `
      );
    });
    this.deletePost();
  }

  deletePost() {
    document.querySelector('#root').addEventListener('click', async (e) => {
      const btn = e.target.closest('.btn');
      if (!btn) {
        return;
      }
      const postId = await e.target.dataset.id;
      const res = await fetch(
        `https://ajax.test-danit.com/api/json/posts/${postId}`,
        {
          method: 'DELETE',
        }
      );

      if (res.ok) {
        e.target.parentElement.remove();
      } else {
        throw new Error();
      }
    });
  }

  render() {
    this.getUsersPosts();
  }
}

export default Card;
