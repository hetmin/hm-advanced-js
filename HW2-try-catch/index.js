// Дано масив books.
// 1. Виведіть цей масив на екран у вигляді списку (тег ul – список має бути згенерований за допомогою Javascript).
// 2. На сторінці повинен знаходитись div з id="root", куди і потрібно буде додати цей список (схоже завдання виконувалось в модулі basic).
// 3. Перед додаванням об'єкта на сторінку потрібно перевірити його на коректність (в об'єкті повинні міститися всі три властивості - author, name, price). Якщо якоїсь із цих властивостей немає, в консолі має висвітитися помилка із зазначенням - якої властивості немає в об'єкті.
// 4. Ті елементи масиву, які не є коректними за умовами попереднього пункту, не повинні з'явитися на сторінці.

const books = [
  {
    author: 'Люсі Фолі',
    name: 'Список запрошених',
    price: 70,
  },
  {
    author: 'Сюзанна Кларк',
    name: 'Джонатан Стрейндж і м-р Норрелл',
  },
  {
    name: 'Дизайн. Книга для недизайнерів.',
    price: 70,
  },
  {
    author: 'Алан Мур',
    name: 'Неономікон',
    price: 70,
  },
  {
    author: 'Террі Пратчетт',
    name: 'Рухомі картинки',
    price: 40,
  },
  {
    author: 'Анґус Гайленд',
    name: 'Коти в мистецтві',
  },
];

books.forEach((item) => {
  try {
    checkBooks(item);
  } catch (error) {
    console.log(error.message);
  }
});

function checkBooks(item) {
  if (!item.name) {
    throw new SyntaxError(`There is no name in the book ${item.author}`);
  }
  if (!item.author) {
    throw new SyntaxError(`There is no author in the book ${item.name}`);
  }
  if (!item.price) {
    throw new SyntaxError(`There is no price in the book ${item.name}`);
  }
}

function createDom(clearArr) {
  const ul = document.createElement('ul');
  const root = document.querySelector('#root');
  root.append(ul);
  clearArr.map((el) => {
    let li = document.createElement('li');
    let author = document.createElement('p');
    let name = document.createElement('p');
    let price = document.createElement('p');
    li.innerHTML = 'Book';
    li.classList.add('book');
    author.innerHTML = `author: ${el.author}`;
    name.innerHTML = `name: ${el.name}`;
    price.innerHTML = `price: ${el.price}`;
    if (el.author && el.name && el.price) {
      ul.append(li);
      li.append(author, name, price);
    }
  });
}

createDom(books);
