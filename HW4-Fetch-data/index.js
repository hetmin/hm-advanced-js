// Отримати список фільмів серії Зоряні війни та вивести на екран список персонажів для кожного з них.

// Технічні вимоги:
// Надіслати AJAX запит на адресу https://ajax.test-danit.com/api/swapi/films та отримати список усіх фільмів серії Зоряні війни
// Для кожного фільму отримати з сервера список персонажів, які були показані у цьому фільмі. Список персонажів можна отримати з властивості characters.
// Як тільки з сервера буде отримана інформація про фільми, відразу вивести список усіх фільмів на екрані. Необхідно вказати номер епізоду, назву фільму, а також короткий зміст (поля episodeId, name, openingCrawl).
// Як тільки з сервера буде отримано інформацію про персонажів будь-якого фільму, вивести цю інформацію на екран під назвою фільму.

// Необов'язкове завдання підвищеної складності
// Поки завантажуються персонажі фільму, прокручувати під назвою фільму анімацію завантаження. Анімацію можна використовувати будь-яку. Бажано знайти варіант на чистому CSS без використання JavaScript.

function getListMovies() {
  fetch('https://ajax.test-danit.com/api/swapi/films')
    .then((response) => response.json())
    .then((data) => {
      return setDom(data);
    });
}

getListMovies();

function setDom(data) {
  data.forEach((el) => {
    const ul = document.createElement('ul');
    document.querySelector('#root').append(ul);
    ul.insertAdjacentHTML(
      'afterbegin',
      `
  <li class="episode"><span class="span">Episode:</span> ${el.episodeId}</li>
  <li class="name"><span class="span">Title:</span> ${el.name}</li>
  <li class="openingCrawl"><span class="span">Opening crawl:</span> ${el.openingCrawl}</li>
  <li class="characters" data-id="${el.id}"><span class="span">Characters:</span> </li>
  `
    );
    const episodeDom = document.querySelector(`[data-id="${el.id}"]`);

    getTest(el.characters, episodeDom);
  });
}

async function getTest(urls, episodeDom) {
  urls.forEach(async (url) => {
    const res = await fetch(url);
    const res2 = await res.json();

    episodeDom.innerHTML += res2.name + ', ';
  });
}
